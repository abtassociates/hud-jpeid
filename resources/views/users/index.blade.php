@extends('layouts.app')

@section('content')
<div class="col-xs-12">
    <div class="panel panel-default">
        <div class="panel-body">
            <h1>Users</h1>

            @if($users->count() > 0)
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th colspan="2" class="text-center">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ join('<br/>', $user->roles->pluck('name')->all()) }}</td>
                                <td class="text-center"><a href="{{ route('users.edit', ['id' => $user->id]) }}">Edit</a></td>
                                <td class="text-center"><a href="{{ route('users.show', ['id' => $user->id]) }}">Delete</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th colspan="2" class="text-center">Actions</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            @else

            @endif

            <div class="text-center">
                {{ $users->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
