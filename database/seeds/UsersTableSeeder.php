<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            ['id' => 1, 'name' => 'Admin'],
            ['id' => 2, 'name' => 'PHA'],
        ]);

        DB::table('users')->insert([
            [
                'id' => 1,
                'first_name' => 'Bongwook',
                'last_name' => 'Lee',
                'email' => 'bongwook_lee@abtassoc.com',
                'password' => bcrypt('S0norama'),
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
            ],
            [
                'id' => 2,
                'first_name' => 'Sam',
                'last_name' => 'Lee',
                'email' => 'samlovescha@gmail.com',
                'password' => bcrypt('S0norama'),
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
            ],
        ]);

        DB::table('user_roles')->insert([
            ['user_id' => 1, 'role_id' => 1],
            ['user_id' => 2, 'role_id' => 2],
        ]);
    }
}
