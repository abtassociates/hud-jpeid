<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const ADMIN = 1;
    const PHA = 2;

    public $timestamps = false;

    protected $fillable = ['name'];
}
