<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'deleted_at',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'user_roles');
    }

    /**
     * Checks to see if the user has the given role ID.
     *
     * @param $roleId
     * @return bool
     */
    public function hasRole($roleId)
    {
        return $this->roles->pluck('id')->contains($roleId);
    }

    /**
     * Convenience function for full name
     *
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }
}
